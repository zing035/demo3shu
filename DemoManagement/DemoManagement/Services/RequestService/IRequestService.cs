﻿using System.Collections.Generic;
using DemoManagement.Models;

namespace DemoManagement.Services.RequestService
{
    public interface IRequestService
    {
        Request GetById(int? id);
        IEnumerable<Request> GetAll();
        void Add(Request request);
        void Delete(int id);
        void Update(Request request);
        void Save();
    }
}