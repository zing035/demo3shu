﻿using System;
using System.Collections.Generic;
using System.Linq;
using DemoManagement.Models;
using DemoManagement.Reponsitory.UserReponsitory;
using DemoManagement.UnitOfWork;

namespace DemoManagement.Services.UserService
{
    public class UserService : IUserService
    {
        private IUnitOfWork _unitOfWork;
        private DemoDBContext _context;
       
        public UserService(IUnitOfWork unitOfWork, DemoDBContext context)
        {
            _unitOfWork = unitOfWork;
            _context = context;
        }
        
        public User GetById(int? id)
        {
            return _unitOfWork.UserReponsitory.GetById(id);
        }

        public IEnumerable<User> GetAll()
        {
            return _unitOfWork.UserReponsitory.GetAll();
        }

        public void Add(User user)
        {
            _unitOfWork.UserReponsitory.Add(user);
        }

        public void Delete(int id)
        {
            _unitOfWork.UserReponsitory.Delete(id);
        }

        public void Update(User user)
        {
            _unitOfWork.UserReponsitory.Update(user);
        }

        public void Save()
        {
            _unitOfWork.UserReponsitory.Save();
        }

        public bool Login(string Email, string Password)
        {
            if (string.IsNullOrEmpty(Email) || String.IsNullOrWhiteSpace(Password))
            {
                return false;
            }

            var user = _context.Users.FirstOrDefault(x => x.Email == Email && x.PassWord == Password);
            if (user == null)
            {
                return false;
            }

            return true;
        }

        public User GetEmail(string email)
        {
            var user = _context.Users.FirstOrDefault(x => x.Email == email);
            return user;
        }
    }
}