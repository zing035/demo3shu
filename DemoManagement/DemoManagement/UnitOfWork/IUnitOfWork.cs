﻿using System;
using DemoManagement.Models;
using DemoManagement.Reponsitory.GenericReponsitory;

namespace DemoManagement.UnitOfWork
{
    public interface IUnitOfWork : IDisposable
    {
        IGenericReponsitory<User> UserReponsitory { get; }
        IGenericReponsitory<Request> RequestReponsitory { get; }
        void Save();
    }
}