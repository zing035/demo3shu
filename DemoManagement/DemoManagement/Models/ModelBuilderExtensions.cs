﻿using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;

namespace DemoManagement.Models
{
    public static class ModelBuilderExtensions
    {
        public static void Seed(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>().HasData(
                new User
                {
                    UserId = 1,
                    FirstName = "Nguyen",
                    LastName = "A",
                    Email = "nguyena123@gmail.com",
                    PassWord = "123456",
                    RoleId = 1,
                    IsActive = true
                }
                );
        }
    }
}