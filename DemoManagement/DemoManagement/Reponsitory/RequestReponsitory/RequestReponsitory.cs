﻿using DemoManagement.Models;
using DemoManagement.Reponsitory.GenericReponsitory;

namespace DemoManagement.Reponsitory.RequestReponsitory
{
    public class RequestReponsitory :  GenericReponsitory<Request>, IRequestReponsitory
    {
        public RequestReponsitory(DemoDBContext context) : base(context)
        {
        }
    }
}