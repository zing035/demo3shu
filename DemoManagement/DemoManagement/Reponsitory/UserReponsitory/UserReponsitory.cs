﻿using DemoManagement.Models;
using DemoManagement.Reponsitory.GenericReponsitory;

namespace DemoManagement.Reponsitory.UserReponsitory
{
    public class UserReponsitory : GenericReponsitory<User>, IUserReponsitory
    {
        public UserReponsitory(DemoDBContext context) : base(context)
        {
        }
    }
}