﻿using System.Collections;
using System.Collections.Generic;

namespace DemoManagement.Reponsitory.GenericReponsitory
{
    public interface IGenericReponsitory<TEntity>
    {
        TEntity GetById(int? id);
        IEnumerable<TEntity> GetAll();
        void Add(TEntity entity);
        void Delete(int id);
        void Update(TEntity entity);
        void Save();
    }
}