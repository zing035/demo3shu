﻿using DemoManagement.Models;
using DemoManagement.Reponsitory.GenericReponsitory;

namespace DemoManagement.Reponsitory.RoleReponsitory
{
    public class RoleReponsitory : GenericReponsitory<Role>, IRoleReponsitory
    {
        public RoleReponsitory(DemoDBContext context) : base(context)
        {
        }
    }
}